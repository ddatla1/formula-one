package com.nbrown.analytics.service;

import com.nbrown.analytics.model.Driver;
import com.nbrown.analytics.model.Result;
import com.nbrown.analytics.model.Team;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ResultsServiceTest {

    private ResultsService resultService;

    @Before
    public void setup() {
        resultService = new ResultsService();
    }

    @Test
    public void getRaceResults_WithValidDocument_ShouldReturnRaceData() {

        // Arrange
        Document doc = resultService.getRaceResultsDocument(new File("./src/test/resources/results.html"));

        // Act
        Result result = resultService.getRaceResults(doc);

        // Assert
        assertThat(result.getTeams(), IsIterableContainingInOrder.contains(getResult().getTeams().toArray()));
        assertThat(result.getDrivers(), IsIterableContainingInOrder.contains(getResult().getDrivers().toArray()));
    }

    @Test
    public void getRaceResults_WithNullDocument_ShouldReturnNull() {

        // Act
        Result result = resultService.getRaceResults(null);

        // Assert
        assertThat(result, equalTo(null));
    }

    private Result getResult() {
        return new Result(getTopTeams(), getTopDrivers());
    }

    private List<Team> getTopTeams() {
        List<Team> teams = new ArrayList<>();

        teams.add(new Team("1", "Mercedes", "585"));
        teams.add(new Team("2", "Ferrari", "530"));
        teams.add(new Team("3", "Red Bull", "362"));
        teams.add(new Team("4", "Renault", "114"));
        teams.add(new Team("5", "Haas F1 Team", "84"));

        return teams;
    }

    private List<Driver> getTopDrivers() {
        List<Driver> drivers = new ArrayList<>();

        drivers.add(new Driver("1", "Lewis Hamilton", "358"));
        drivers.add(new Driver("2", "Sebastian Vettel", "294"));
        drivers.add(new Driver("3", "Kimi Räikkönen", "236"));
        drivers.add(new Driver("4", "Valtteri Bottas", "227"));
        drivers.add(new Driver("5", "Max Verstappen", "216"));
        drivers.add(new Driver("6", "Daniel Ricciardo", "146"));
        drivers.add(new Driver("7", "Nico Hülkenberg", "69"));
        drivers.add(new Driver("8", "Sergio Pérez", "57"));
        drivers.add(new Driver("9", "Kevin Magnussen", "53"));
        drivers.add(new Driver("10", "Fernando Alonso", "50"));

        return drivers;
    }
}
