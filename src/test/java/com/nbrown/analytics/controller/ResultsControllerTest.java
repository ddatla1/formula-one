package com.nbrown.analytics.controller;

import com.nbrown.analytics.model.Driver;
import com.nbrown.analytics.model.Result;
import com.nbrown.analytics.model.Team;
import com.nbrown.analytics.service.ResultsService;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ResultsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private InternalResourceViewResolver viewResolver;

    @Mock
    private ResultsService resultsService;

    @InjectMocks
    private ResultsController resultsController;

    @Mock
    private Document document;

    private Result result;

    @Before
    public void setup() {
        viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");

        mockMvc = MockMvcBuilders.standaloneSetup(resultsController)
                .setViewResolvers(viewResolver).build();

        List<Team> teams = Arrays.asList(new Team("1", "Mercedes", "585"));
        List<Driver> drivers = Arrays.asList(new Driver("1", "Lewis Hamilton", "358"));
        result = new Result(teams, drivers);
    }

    @Test
    public void getResults_ShouldReturnTopStandingResults() throws Exception {

        // Arrange
        when(this.resultsService.getRaceResultsDocument(anyString())).thenReturn(document);
        when(this.resultsService.getRaceResults(document)).thenReturn(result);

        // Act and Assert
        this.mockMvc.perform(get("/results"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("teams[0].name").value("Mercedes"))
                .andExpect(jsonPath("drivers[0].name").value("Lewis Hamilton"));

        verify(this.resultsService, times(1)).getRaceResultsDocument(anyString());
        verify(this.resultsService, times(1)).getRaceResults(any());
    }

    @Test
    public void startup_ShouldRedirectPathToHome() throws Exception {
        this.mockMvc.perform(get("/"))
                .andExpect(redirectedUrl("/home"))
                .andExpect(view().name("redirect:/home"));
    }

    @Test
    public void home_ShouldInvokeHomePage() throws Exception {
        this.mockMvc.perform(get("/home"))
                .andExpect(view().name("home"));
    }

}
