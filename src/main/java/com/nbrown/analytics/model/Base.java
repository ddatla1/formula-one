package com.nbrown.analytics.model;

import java.util.Objects;

public class Base {
    private String position;
    private String name;
    private String points;

    public Base(String position, String name, String points) {
        this.position = position;
        this.name = name;
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Base base = (Base) o;
        return Objects.equals(position, base.position) &&
                Objects.equals(name, base.name) &&
                Objects.equals(points, base.points);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, name, points);
    }

    public String getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public String getPoints() {
        return points;
    }
}
