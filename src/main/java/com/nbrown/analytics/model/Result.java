package com.nbrown.analytics.model;

import java.util.List;

public class Result {
    private List<Team> teams;
    private List<Driver> drivers;

    public Result(List<Team> teams, List<Driver> drivers) {
        this.teams = teams;
        this.drivers = drivers;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }
}
