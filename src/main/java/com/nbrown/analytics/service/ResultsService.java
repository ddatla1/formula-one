package com.nbrown.analytics.service;

import com.nbrown.analytics.model.Driver;
import com.nbrown.analytics.model.Result;
import com.nbrown.analytics.model.Team;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ResultsService {

    private static final String USER_AGENT = "Mozilla";

    private static final String DRIVER_TOP_STANDING_TABLE_SELECTOR = "table.msr_season_driver_results";
    private static final String TEAM_TOP_STANDING_TABLE_SELECTOR = "table.msr_season_team_results";

    private static final String ROW_SELECTOR = "tr:has(td.msr_pos)";
    private static final String POSITION_SELECTOR = "td.msr_pos";
    private static final String POINTS_SELECTOR = "td.msr_total";

    private static final String DRIVER_NAME_SELECTOR = "td.msr_driver";
    private static final String TEAM_NAME_SELECTOR = "td.msr_team";

    private static final int TOP_DRIVERS_LIST_LIMIT = 10;
    private static final int TOP_TEAMS_LIST_LIMIT = 5;

    private static Logger logger = LoggerFactory.getLogger(ResultsService.class);

    public Result getRaceResults(Document document) {
        if (document != null) {
            return new Result(getTopStandingTeams(document), getTopStandingDrivers(document));
        }
        return null;
    }

    public Document getRaceResultsDocument(String url) {
        if (url != null) {
            try {
                return Jsoup.connect(url).userAgent(USER_AGENT).get();
            } catch (IOException e) {
                logger.error("An error has occurred connecting the url {}", url, e);
            }
        }
        return null;
    }

    public Document getRaceResultsDocument(File file) {
        if (file != null) {
            try {
                return Jsoup.parse(file, "UTF-8");
            } catch (IOException e) {
                logger.error("An error has occurred parsing the file {}", file, e);
            }
        }
        return null;
    }

    private List<Driver> getTopStandingDrivers(Document document) {
        List<Driver> drivers = new ArrayList<>();

        Element table = document.select(DRIVER_TOP_STANDING_TABLE_SELECTOR).first();
        Elements rows = table.select(ROW_SELECTOR);

        for (int row = 0; row < TOP_DRIVERS_LIST_LIMIT; row++) {
            String position = rows.get(row).select(POSITION_SELECTOR).text();
            String driver = rows.get(row).select(DRIVER_NAME_SELECTOR).text();
            String points = rows.get(row).select(POINTS_SELECTOR).text();

            Driver player = new Driver(position, driver, points);
            drivers.add(player);
        }
        return drivers;
    }

    private List<Team> getTopStandingTeams(Document document) {
        List<Team> teams = new ArrayList<>();

        Element table = document.select(TEAM_TOP_STANDING_TABLE_SELECTOR).first();
        Elements rows = table.select(ROW_SELECTOR);

        for (int row = 0; row < TOP_TEAMS_LIST_LIMIT; row++) {
            String position = rows.get(row).select(POSITION_SELECTOR).text();
            String teamName = rows.get(row).select(TEAM_NAME_SELECTOR).text();
            String points = rows.get(row).select(POINTS_SELECTOR).text();

            Team team = new Team(position, teamName, points);
            teams.add(team);
        }
        return teams;
    }
}
