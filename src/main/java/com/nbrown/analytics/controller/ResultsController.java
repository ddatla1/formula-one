package com.nbrown.analytics.controller;

import com.nbrown.analytics.model.Result;
import com.nbrown.analytics.service.ResultsService;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ResultsController {

    @Autowired
    public ResultsController(ResultsService resultsService) {
        this.resultsService = resultsService;
    }

    private ResultsService resultsService;

    @GetMapping("/")
    public String startup() {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/results")
    public @ResponseBody Result results() {
        Document document = resultsService.getRaceResultsDocument(
                "https://www.f1-fansite.com/f1-results/2018-f1-championship-standings");
        return resultsService.getRaceResults(document);
    }
}
