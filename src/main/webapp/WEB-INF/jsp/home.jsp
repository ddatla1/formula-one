<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
    <head>
        <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/home.css"/>
    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <img src="images/formulaone.png" class="pull-left" height="50" width="50">
                <div class="navbar-header">
                    <a class="navbar-brand center" href="#">Formula One - Season 2018 Results</a>
                </div>
                <img src="images/formulaone.png" class="pull-right" height="50" width="50">
            </div>
        </nav>

        <br>
        <br>

        <div class="container">
            <div class="form-group">
                <table>
                    <tr>
                        <td><h4>Welcome to Formula One Home Page - Top Standings of the Season</h4></td>
                    </tr>
                </table>
                <button type="submit" id="btn-results" class="btn btn-primary btn-lg pull-right">Load Results</button>
            </div>

            <br>

            <div id="ajax-request-confirmation" class="text-success" align="center"></div>
            <div id="error-response"></div>

            <h4>Top 10 Drivers of 2018</h4>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Position</th>
                    <th scope="col">Driver</th>
                    <th scope="col">Points</th>
                </tr>
                </thead>

                <tbody id="ajax-data-drivers">

                </tbody>
            </table>

            <br>

            <h4>Top 5 Teams of 2018</h4>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Position</th>
                    <th scope="col">Team</th>
                    <th scope="col">Points</th>
                </tr>
                </thead>

                <tbody id="ajax-data-teams">

                </tbody>
            </table>

            <br><br><br><br>
        </div>

        <script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/ajax.js"></script>
    </body>
    <nav class="navbar-wrapper navbar-inverse text-center">
        <div class="container text-center">
            <div class="navbar-text">&copy; 2018 by Software Development Assignment. All rights reserved.</div>
            <div class="navbar-text pull-right">Verion 0.0.1-SNAPHOT</div>
        </div>
    </nav>
</html>