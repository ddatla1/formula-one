$(document).ready(function () {

    ajaxLoadResults();

    $("#btn-results").click(function () {
        ajaxLoadResults();
    });

});

function ajaxLoadResults() {

    $("#ajax-request-confirmation")
        .html("<h4 id='fadeout'>An ajax request has been made.</h4>");

    $("#fadeout").fadeOut("slow")

    $("#btn-results").prop("disabled", true);

    $.ajax({

        type: "GET",
        contentType: "application/json",
        url: "/results",
        cache: false,
        timeout: 600000,
        success: function (data) {

            var teams = buildGridRows(data.teams);
            $('#ajax-data-teams').html(teams);

            var drivers = buildGridRows(data.drivers);
            $('#ajax-data-drivers').html(drivers);

            $("#btn-results").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>" + e.responseText + "</pre><br>";
            $('#error-response').html(json);

            console.log("ERROR : ", e);
            $("#btn-results").prop("disabled", false);

        }

    });

}

function buildGridRows(data) {

    var rows = "";

    $.each(data, function (i, obj) {
        rows = rows + "<tr>"
            + "<td>" + (obj).position + "</td>"
            + "<td>" + (obj).name + "</td>"
            + "<td>" + (obj).points + "</td>"
            + "</tr>"
    });

    return rows;
}
