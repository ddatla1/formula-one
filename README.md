## Formula One Season Results 

This application will display top drivers and teams of Formula One Season 2018.

### Software and Tools used to Build the Application

- `Java 8`
- `Maven 3.5.4`
- `Spring Boot Web 2.1.0-RELEASE`
- `macOs High Sierra`
- `IntelliJ IDEA`

### Compile and Test

- `mvn compile` to compile application
- `mvn test` to run unit tests

## Running Web Application

Two ways to run the application
   
- Either run `mvn spring-boot:run` on command line at project root location
- Or execute `mvn packge` and then `java -jar target/formula-one-0.0.1-SNAPSHOT.jar`

After successful execution of the commands above, you should be able to access 
the web application at `http:\\localhost:9090`

On initial page load, an ajax request will be made to get formula one data 
from the link `https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/` and 
will be populated in the grid.

There is a button called `Load Results` can be clicked to make an ajax request to the service 
again to reload the data when needed.

## Additional Information

This application has been developed using MVC concepts, 
a service is responsible to retrieve the data that we are interested,
a controller to an act as an api to connect with the client to facilitate the requests,
and a view to format and render the data on the page.

Testing has been done using test data at the location 'test\resources\results.html'
to simulate real time process to test the software components involved.
